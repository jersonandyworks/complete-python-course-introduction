import re

def main():
    file = open("raven.txt")
    pattern = re.compile('(Len|Neverm)ore',re.IGNORECASE)
    for x,line in enumerate(file):
        match = re.search(pattern,line)
        if match:
            print( pattern.sub('###',line) )

if __name__ == '__main__':main()