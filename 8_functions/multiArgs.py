def main():
    try:
        player("John",45,"attacker","Zeus","Another param")
    except TypeError as e:
        print("{}".format(e))

def player(*args):
    numArgs = len(args)
    if numArgs > 4:
        raise TypeError("Only 4 arguments are required!")
    else:
        print(args[0],args[1],args[2],args[3])
def sum(num1,num2):
    print(num1 + num2)
if __name__ == '__main__':main()