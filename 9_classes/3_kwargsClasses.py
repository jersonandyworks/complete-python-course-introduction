class Instructor:
    def __init__(self,**kwargs):
        self.variables = kwargs

    def set_variable(self,k,v):
        self.variables[k] = v

    def get_variable(self,k):
       return self.variables.get(k,None)
def main():
    ictInstructor = Instructor(lastname="Doe")
    ictInstructor.set_variable('name','john')
    print(ictInstructor.get_variable('lastname'))
if __name__ == '__main__':main()