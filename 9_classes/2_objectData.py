class Instructor:
    def __init__(self,fname=None,lname=None,age=None,dept=None,ratePerHour=95):
        self.firstname = fname
        self.lastname = lname
        self.age = age
        self.department = dept
        self.ratePerHour = ratePerHour

    def setProfile(self,fname,lname,age,dept,ratePerHour=95):
        self.firstname = fname
        self.lastname = lname
        self.age = age
        self.department = dept
        self.ratePerHour = ratePerHour

    def getProfileInfo(self):
        info = {"firstname":self.firstname,"lastname":self.lastname,
                "age":self.age,"department":self.department,"rate":self.ratePerHour}
        return info
def main():
    ictInstructor = Instructor()
    ictInstructor.setProfile("John","Doe",26,"ICT")
    profile = ictInstructor.getProfileInfo()
    for p in profile:
        print (profile[p],end=" ")
if __name__ == '__main__':main()