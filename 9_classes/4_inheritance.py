class Vehicle:
    def initialize(self):
        print("All belongs to this parent class!")

class Car(Vehicle):
    def __init__(self,**kwargs):
        self.variable = kwargs
    def set_variable(self,k,v):
        self.variable[k] = v
    def get_variable(self,k):
        return self.variable.get(k,None)

honda = Car()
honda.initialize()
honda.set_variable('tire','Good year')
print(honda.get_variable('tire'))

