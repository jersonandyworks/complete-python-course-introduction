class Vehicle:
    def initialize(self):
        print("All belongs to this parent class!")

class Car(Vehicle):
    def __init__(self,**kwargs):
        self.variable = kwargs
    def set_variable(self,k,v):
        self.variable[k] = v
    def get_variable(self,k):
        return self.variable.get(k,None)

def main():
    print(goCar(Car(),'honda'))

def goCar(car,value):
    car.set_variable('car',value)
    return car.get_variable('car')
if __name__ == '__main__':main()
