def main():
    try:
        fh = open("filename.txt")
    except IOError as e:
        print(e)
        #print("Could not open the file!")
    else:
        for line in fh.readlines():print(line.strip())
if __name__ == '__main__':
    main()

