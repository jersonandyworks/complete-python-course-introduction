name = "    Timothy Aaron M Barrios    "
print(name.upper())
print(name.isalpha())
print(name.capitalize())
print(name.lower())
print(name.islower())
print(name.find('Aaron')) #finds a specific string
print(name.replace('Timothy','Zach'))
print(name.strip()) #removes white space from the beginning to end of the string
print(name.lstrip()) #removes white space from the beginning of the string
print(name.rstrip()) #removes white space from the end of the string
name = name.replace(' ','')
print(name.isalpha())

