def isPrime(n):
	n = n % 2
	if n == 1:
		return False
	else:
		return True

def numbers(n=1):
	while(True):
		if isPrime(n): yield n
		n +=1


for i in numbers():
	if i > 100: break
	print(i)